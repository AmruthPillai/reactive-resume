// React Queries
export const FONTS_QUERY = 'fonts';
export const RESUMES_QUERY = 'resumes';

// Date Formats
export const FILENAME_TIMESTAMP = 'DDMMYYYYHHmmss';

// Links
export const DONATION_URL = 'https://www.buymeacoffee.com/AmruthPillai';
export const GITHUB_URL = 'https://github.com/AmruthPillai/Reactive-Resume';
export const GITHUB_ISSUES_URL = 'https://github.com/AmruthPillai/Reactive-Resume/issues/new/choose';
